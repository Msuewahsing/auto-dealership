import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-secondary">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Dealership</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturer">Manufacturers</NavLink>
              <NavLink className="nav-link" to="/manufacturer/new">Create a Manufacturer</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/model">Models</NavLink>
              <NavLink className="nav-link" to="/model/new">Create a Model</NavLink>
          </li>
          <li>
              <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
              <NavLink className="nav-link" to="/automobiles/new">Create an Automobile</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
              <NavLink className="nav-link" to="/salespeople/new">Add a Salesperson</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
              <NavLink className="nav-link" to="/customers/new">Add a Customer</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/">Technicians</NavLink>
              <NavLink className="nav-link" to="/technicians/new">Create a Technician</NavLink>
          </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/sales">Sales</NavLink>
              <NavLink className="nav-link" to="/sales/new">Add a Sale</NavLink>
              <NavLink className="nav-link" to="/sales/history">Sales History</NavLink>
          </li>
           <li>
              <NavLink className="nav-link" to="/appointments">Service Appointments</NavLink>
              <NavLink className="nav-link" to="/appointments/new">Create a service appointment</NavLink>
              <NavLink className="nav-link" to="/appointments/history">Service History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
