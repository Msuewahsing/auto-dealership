import React from "react";
import {useState,useEffect} from 'react'

function MainPage() {
  const [models,setModels] = useState([])

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }
  useEffect(() => {
    fetchData();
  },[]);

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Dealership</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <div id="carouselExampleInterval" class="carousel slide" data-bs-ride="carousel">
          <div className="carousel-inner">
            {models.map((model,index) => (
            <div className={`carousel-item ${index === 0 ? 'active' : ''}`} data-bs-interval="5000" key={model.id} >
              <img src={model.picture_url} className="d-block w-100" alt="..."/>
            </div>
            ))}
          </div>
          <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
