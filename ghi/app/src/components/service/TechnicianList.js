import React, { useState, useEffect } from 'react'

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    const getTechnicians = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

    useEffect(() => {
        getTechnicians();
    }, []);

    const deleteTechnician = async (id) => {
        const deleteTechnicianUrl =`http://localhost:8080/api/technicians/${id}`;

        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type' : 'application/json'
            },
        }
        const response = await fetch (deleteTechnicianUrl,fetchConfig)

        if (response.ok){
            setTechnicians(technicians.filter(technician => technician.id !== id))
        }
    }

    return(
            <div id="form-row" className='row'>
                <div className='container-responsive shadow p-4'>
                    <h1 className='text-center'>Technicians</h1>
                    <table className='table table-striped'>
                        <thead>
                            <tr>
                                <th>Employee ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Remove Technician</th>
                            </tr>
                        </thead>
                        <tbody>
                            {technicians.map((technician) => (
                            <tr key={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td><button type="button" className="btn btn-danger" onClick={()=>deleteTechnician(technician.id)}>Delete</button></td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    };
export default TechnicianList;
