import React, { useState, useEffect } from 'react'

function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);

    const getAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            const updated_data = data.appointments.filter(appointment => appointment.status === "Created");
            setAppointments(updated_data);
        }
    };

    useEffect(() => {
        getAppointments();
    }, []);

    const finishedAppointment = async (id) =>{
        const finishedAppointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`

        const fetchConfig = {
            method: "PUT",
            headers: {
                'Content-Type' : 'application/json'
            },
        }
        const response = await fetch(finishedAppointmentUrl,fetchConfig)
        if(response.ok){
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    }

    const cancelledAppointment = async (id) =>{
        const cancelledAppointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
        const fetchConfig = {
            method: "PUT",
            headers: {
                'Content-Type' : 'application/json'
            },
        }
        const response = await fetch(cancelledAppointmentUrl,fetchConfig)
        if(response.ok){
            setAppointments(appointments.filter(appointment => appointment.id !== id));
        }
    }
    return(
        <div id="form-row" className='row'>
            <div className='container-responsive shadow p-4'>
                <h1 className='text-center'>Appointments</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Vin</th>
                            <th>VIP</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map((appointment) => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.sold ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date).toLocaleTimeString()}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                <div className='btn-toolbar'>
                                    <button type="submit" className="btn btn-danger mx-2" onClick={()=>cancelledAppointment(appointment.id)}>Canceled</button>
                                    <button type="submit" className="btn btn-success" onClick={()=>finishedAppointment(appointment.id)}>Finished</button>
                                </div>
                            </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
        )
    };
export default AppointmentsList;
