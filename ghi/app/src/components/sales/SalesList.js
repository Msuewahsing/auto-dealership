import React, { useState, useEffect } from 'react';

export default function SalesList() {
    const [sales, setSales] = useState([]);

    const getSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales_records);
        }
    };

    useEffect(() => {
        getSales();
    }, []);

    const deleteSales = async (id) => {
        const deleteUrl = `http://localhost:8090/api/sales/${id}/`;
            const response = await fetch(deleteUrl, { method: 'DELETE' });
            if (response.ok) {
                setSales(sales.filter(s => s.id !== id));
        }
    };

    return (
        <div id="form-row" className='row'>
            <div className='container-responsive shadow p-4'>
                <h1 className='text-center'>Sales</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Salesperson Employee ID</th>
                            <th>Salesperson Name</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.map((sale) => (
                        <tr key={sale.id}>
                            <td>{sale.emp_no}</td>
                            <td>{sale.sr_name}</td>
                            <td>{sale.cust_name}</td>
                            <td>{sale.import_vin}</td>
                            <td>{sale.price}</td>
                            <td>
                                <button className='btn btn-danger' onClick={() => deleteSales(sale.id)}>Delete</button>
                            </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
};
