import React, { useState, useEffect } from 'react';

export default function CustomerList() {
    const [customers, setCustomers] = useState([]);
    const getCustomer = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    };

    useEffect(() => {
        getCustomer();
    }, []);

    const deleteCustomer = async (id) => {
        const deleteUrl = `http://localhost:8090/api/customers/${id}/`;
            const response = await fetch(deleteUrl, { method: 'DELETE' });
            if (response.ok) {
                setCustomers(customers.filter(c => c.id !== id));
        }
    };

    return (
        <div id="form-row" className='row'>
            <div className='container-responsive shadow p-4'>
                <h1 className='text-center'>Customers</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        {customers.map((customer) => (
                        <tr key={customer.id}>
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.phone_no}</td>
                            <td>{customer.address}</td>
                            <td>
                                <button className='btn btn-danger' onClick={() => deleteCustomer(customer.id)}>Delete</button>
                            </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
};
