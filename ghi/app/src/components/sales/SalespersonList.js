import React, { useState, useEffect } from 'react';

export default function SalespersonList() {
    const [salespersons, setSalespersons] = useState([]);

    const getEmployee = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.sales_persons);
        }
    };

    useEffect(() => {
        getEmployee();
    }, []);

    const deleteEmployee = async (id) => {
        const deleteUrl = `http://localhost:8090/api/salespeople/${id}/`;
            const response = await fetch(deleteUrl, { method: 'DELETE' });
            if (response.ok) {
                setSalespersons(salespersons.filter(s => s.id !== id));
        }
    };

    return (
        <div id="form-row" className='row'>
            <div className='container-responsive shadow p-4'>
                <h1 className='text-center'>Salespeople</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Employee_id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salespersons.map((salesperson) => (
                        <tr key={salesperson.id}>
                            <td>{salesperson.employee_no}</td>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                            <td>
                                <button className='btn btn-danger' onClick={() => deleteEmployee(salesperson.id)}>Delete</button>
                            </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
};
