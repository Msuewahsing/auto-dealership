from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "id",
        "sold"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date",
        "customer",
        "reason",
        "technician",
        "vin",
        "id",
        "status",
        "sold",

    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
