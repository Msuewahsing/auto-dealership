from django.urls import path
from .views import (
    api_technician,
    api_technicians,
    api_appointment,
    api_cancelled_appointments,
    api_finished_appointments,
    api_appointments,
)

urlpatterns = [
    path("technicians/", api_technician, name="api_technician"),
    path("technicians/<int:id>/", api_technicians, name="api_technicians"),
    path("appointments/", api_appointment, name="api_appointment"),
    path("appointments/<int:id>/", api_appointments, name="api_appointments"),
    path(
        "appointments/<int:id>/cancel/",
        api_cancelled_appointments,
        name="api_cancelled_appointments",
    ),
    path(
        "appointments/<int:id>/finish/",
        api_finished_appointments,
        name="api_finished_appointments",
    ),
]
