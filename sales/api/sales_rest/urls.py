from django.urls import path
from .views import (
    salesperson_list,
    customer_list,
    salesperson_detail,
    customer_detail,
    salesrec_list,
    salesrec_detail,
    vins_list,
)
urlpatterns = [
    path("salespeople/", salesperson_list, name="salesperson_list"),
    path("salespeople/<int:id>/", salesperson_detail, name="salesperson_detail"),
    path("customers/", customer_list, name="customer_list"),
    path("customers/<int:id>/", customer_detail, name="customer_detail"),
    path("sales/", salesrec_list, name="salesrec_list"),
    path("sales/<int:id>/", salesrec_detail, name="salesrec_detail"),
    path("vins/", vins_list, name="vins_list"),
]
